<?php
namespace Happy\Coding\Controller;

/*
 * This file is part of the Happy.Coding package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;

class ApiController extends ActionController
{

    /**
     * @return string
     */
    public function indexAction()
    {
        $this->response->setContentType('application/json');
        return json_encode(['foo' => 'bar']);
    }
}
