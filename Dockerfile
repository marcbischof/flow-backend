# ---------------------------------------------- Build Time Arguments --------------------------------------------------
ARG PHP_VERSION="8.0"
ARG NGINX_VERSION="1.20.1"
ARG COMPOSER_VERSION="2.1"
ARG IMAGE_DEPS="fcgi tini icu-dev gettext curl"
ARG RUNTIME_DEPS="zip"

# --------------------------------------------------- Build Images -----------------------------------------------------

FROM composer:${COMPOSER_VERSION} as composer

# ======================================================================================================================
#                                                   --- Base ---
# ---------------  This stage install needed extenstions, plugins and add all needed configurations  -------------------
# ======================================================================================================================

FROM php:${PHP_VERSION}-fpm-alpine AS base

# Required Args ( inherited from start of file, or passed at build )
ARG IMAGE_DEPS
ARG RUNTIME_DEPS

# Maintainer label
LABEL maintainer="marc.bischof@swisscom.com"

# ------------------------------------- Install Packages Needed Inside Base Image --------------------------------------

RUN IMAGE_DEPS="tini gettext"; \
    RUNTIME_DEPS="fcgi"; \
    apk add --no-cache ${IMAGE_DEPS} ${RUNTIME_DEPS}

# ---------------------------------------- Install / Enable PHP Extensions ---------------------------------------------

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions opcache gd igbinary imagick mysqli pdo_mysql pdo_pgsql pdo_sqlite redis zip

# ------------------------------------------------- Permissions --------------------------------------------------------

# - Clean bundled config/users & recreate them with UID 1000 for docker compatability in dev container.
# - Create composer directories (since we run as non-root later)
RUN deluser --remove-home www-data && addgroup -g 1000 -S app && adduser -u 1000 -S -D -G app app && \
    rm -rf /var/www /usr/local/etc/php-fpm.d/* && \
    mkdir -p /var/www/.composer /app \
    && chown -R app:app /app /var/www/.composer

# ------------------------------------------------ PHP Configuration ---------------------------------------------------

ENV PHP_OPCACHE_ENABLE="1"
ENV PHP_OPCACHE_MEMORY_CONSUMPTION="256"
ENV PHP_OPCACHE_MAX_ACCELERATED_FILES="20000"
ENV PHP_OPCACHE_REVALIDATE_FREQUENCY="0"
ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS="0"

# Add Default Config
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# Add in Base PHP Config
COPY docker/php/base/conf.d/base-*   $PHP_INI_DIR/conf.d

# ---------------------------------------------- PHP FPM Configuration -------------------------------------------------

ENV PHP_FPM_PM="ondemand"
ENV PHP_FPM_MAX_CHILDREN="5"
ENV PHP_FPM_START_SERVERS="2"
ENV PHP_FPM_MIN_SPARE_SERVERS="1"
ENV PHP_FPM_MAX_SPARE_SERVERS="2"
ENV PHP_FPM_MAX_REQUESTS="1000"

# PHP-FPM config
COPY docker/php/base/php-fpm.d/*.conf  /usr/local/etc/php-fpm.d/


# --------------------------------------------------- Scripts ----------------------------------------------------------

COPY docker/php/base/*-base          \
     docker/php/base/healthcheck-*   \
     # to
     /usr/local/bin/

RUN  chmod +x /usr/local/bin/*-base /usr/local/bin/healthcheck-*

# ---------------------------------------------------- Composer --------------------------------------------------------

COPY --from=composer /usr/bin/composer /usr/bin/composer

# ----------------------------------------------------- MISC -----------------------------------------------------------

WORKDIR /app
USER app

# Common PHP Frameworks Env Variables
ENV FLOW_CONTEXT Production
ENV FLOW_ROOTPATH /app

# Validate FPM config (must use the non-root user)
RUN php-fpm -t

# ---------------------------------------------------- HEALTH ----------------------------------------------------------

HEALTHCHECK CMD ["healthcheck-liveness"]

# -------------------------------------------------- ENTRYPOINT --------------------------------------------------------

ENTRYPOINT ["entrypoint-base"]
CMD ["php-fpm"]

# ======================================================================================================================
#                                               --- Dependencies ---
# ---------------  This stage will install composer runtime dependencies and install app dependencies.  ----------------
# ======================================================================================================================

# ------------------------------------------- Development Dependencies -------------------------------------------------

# ------------------------------------------- Production Dependencies --------------------------------------------------

FROM composer as dependencies

ARG PHP_VERSION
ARG COMPOSER_FLAGS="--no-dev --optimize-autoloader --no-interaction --no-progress --ansi --prefer-dist"

WORKDIR /app
# Copy Dependencies files
COPY composer.json composer.json
COPY composer.lock composer.lock
COPY DistributionPackages DistributionPackages
COPY Configuration Configuration

# Set PHP Version of the Image
RUN composer config platform.php ${PHP_VERSION}

# Install Dependencies
RUN composer install $COMPOSER_FLAGS

# ======================================================================================================================
# ==============================================  PRODUCTION IMAGE  ====================================================
#                                                   --- PROD ---
# ======================================================================================================================

FROM base AS app

USER root

# Copy Prod Scripts
COPY docker/php/prod/*-prod /usr/local/bin/
RUN  chmod +x /usr/local/bin/*-prod

# Copy PHP Production Configuration
COPY docker/php/prod/conf.d/prod-*   $PHP_INI_DIR/conf.d/

USER app

# ----------------------------------------------- Production Config -----------------------------------------------------

# Copy Dependencies
COPY --chown=app:app --from=dependencies /app/bin /app/bin
COPY --chown=app:app --from=dependencies /app/Configuration /app/Configuration
COPY --chown=app:app --from=dependencies /app/DistributionPackages /app/DistributionPackages
COPY --chown=app:app --from=dependencies /app/Packages /app/Packages
COPY --chown=app:app --from=dependencies /app/Web /app/Web
COPY --chown=app:app --from=dependencies /app/composer.json /app/composer.json
COPY --chown=app:app --from=dependencies /app/composer.lock /app/composer.lock
COPY --chown=app:app --from=dependencies /app/flow /app/flow

# Run Composer Install again
# ( this time to run post-install scripts, autoloader, and post-autoload scripts using one command )
RUN post-build-base && post-build-prod

ENTRYPOINT ["entrypoint-prod"]
CMD ["php-fpm"]

# ======================================================================================================================
# ======================================================================================================================
#                                                  --- NGINX ---
# ======================================================================================================================
# ======================================================================================================================
FROM nginx:${NGINX_VERSION}-alpine AS nginx

RUN rm -rf /var/www/* /etc/nginx/conf.d/* && \
    addgroup -g 1000 -S app && adduser -u 1000 -S -D -G app app
COPY docker/nginx/nginx-*   /usr/local/bin/
COPY docker/nginx          /etc/nginx/
RUN chown -R app:app /etc/nginx/ && chmod +x /usr/local/bin/nginx-*

# The PHP-FPM Host
## Localhost is the sensible default assuming image run on a k8S Pod
ENV PHP_FPM_HOST "localhost"
ENV PHP_FPM_PORT "9000"

# For Documentation
EXPOSE 8080

# Switch User
USER app

# Add Healthcheck
HEALTHCHECK CMD ["nginx-healthcheck"]

# Add Entrypoint
ENTRYPOINT ["nginx-entrypoint"]

# ======================================================================================================================
#                                                 --- NGINX PROD ---
# ======================================================================================================================

FROM nginx AS web

# Copy Public folder + Assets that's going to be served from Nginx
# COPY --chown=app:app Web /app/Web
